var str = "My,name,is,Jakub"
var characterRemover = function(str, character){
    var newStr = str.split(character).join(" ")
    return newStr
}
module.exports = {
    characterRemover
}