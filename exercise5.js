var arr = ["me", "myself", "I"]

var removeFirstAndLast = function(arr){
    arr.splice(0,1)
    arr.splice(-1)
    return arr
}
module.exports = {
    removeFirstAndLast
}