var isArrayFunc = function(argument){
    return argument.constructor === Array
}
module.exports = {
    isArrayFunc
}
